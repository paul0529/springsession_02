package cn.yunhe.spring.session.controller;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {

	@RequestMapping("/get")
	public String showUser() {
		return "user";
	}

	@RequestMapping("/login")
	@ResponseBody
	public void login(HttpSession session) {
		session.setAttribute("username", "zhangshunli");
	}

	@RequestMapping("/logout")
	@ResponseBody
	public void logout(HttpSession session) {
		session.invalidate();
	}
}
