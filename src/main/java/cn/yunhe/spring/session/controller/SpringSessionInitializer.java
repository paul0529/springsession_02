package cn.yunhe.spring.session.controller;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * 用于向SERVLET容器中添加springSessionRepositoryFilter
 * 
 * @author ZSL
 *
 */
public class SpringSessionInitializer extends AbstractHttpSessionApplicationInitializer {
}
