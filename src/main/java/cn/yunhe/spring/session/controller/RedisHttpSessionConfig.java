package cn.yunhe.spring.session.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 用来启用RedisHttpSession功能，并向Spring容器中注册一个RedisConnectionFactory
 * 
 * @author ZSL
 *
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 7200)  
public class RedisHttpSessionConfig {  
  
	@Value("${redis.host}")    
	private String redisIp;
	@Value("${redis.pasword}")    
	private String redisPwd;
	
    @Bean  
    public RedisConnectionFactory connectionFactory() {  
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory();  
        connectionFactory.setHostName(redisIp);  
        connectionFactory.setPort(6379);  
        connectionFactory.setPassword(redisPwd);
        return connectionFactory;  
    }  
    
}  